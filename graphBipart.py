import numpy as np
import llist
import copy
class Node:
  index = 0
  gain = 0
  prev = None
  next = None
  links = []
  is_in_bucket = False
  def __init__(self, index, links):
    self.index = index
    self.links = links

class Node_list(object):
    def __init__(self):
        self.head = None
        self.tail = None
        self.count = 0

    def append(self, node):
        # Append an item 
        if self.head is None:
            self.head = node
            self.tail = self.head
        else:
            node.prev = self.tail
            self.tail.next = node
            self.tail = node
        node.is_in_bucket = True
        self.count += 1
        
    def pop(self):
      if self.tail != None:
        tmp = self.tail
        if(self.tail.prev != None): #There is at least one more item before the tail
          self.tail.prev.next = None
          self.tail = self.tail.prev
        else:
          self.tail = None
          self.head = None
        self.count -= 1
        tmp.is_in_bucket = False
        return tmp
      else:
        print("popping empty list")

    def remove_node(self, node):
      if node.is_in_bucket:
        if node.prev == None and node.next == None:
          self.tail = None
          self.head = None
        if node.next == None: #Node is last item
          self.tail = node.prev
        else:
          node.next.prev = node.prev

        if node.prev == None: #Node is first item
          self.head = node.next
        else:
          node.prev.next = node.next
        node.is_in_bucket = False
        self.count -= 1
      else:
        print("Removing node that is not in a bucket")

class Graph:
  nodes = []
  maxLinks = 0
  def __init__(self):
    self.readGraph()

  def readGraph(self):
    f = open("test.txt", "r")
    for index, line in enumerate(f):
      split = line.split()
      links = list(map(int, split[3:]))
      self.maxLinks = max(len(links), self.maxLinks)
      self.nodes.append(Node(index + 1, links))

class Model:
  def __init__(self):
    self.Graph = Graph()
    self.maxLinks = self.Graph.maxLinks
    self.currentSolution = self.generate_random_solution()
    self.cuts = self.calculate_cuts()

  def calculate_cuts(self):
    cut = 0
    for node in self.Graph.nodes:
      own_value = self.currentSolution[node.index -1]
      for link in node.links:
        if own_value != self.currentSolution[link -1]:
          cut += 1
    return int(cut / 2)

  
  def generate_random_solution(self):
    node_amount = len(self.Graph.nodes)
    solution = np.repeat(0, node_amount)
    solution[int(node_amount/2):] = np.repeat(1, int(node_amount/2))
    np.random.seed(1)
    np.random.shuffle(solution)
    return solution
  
  def generate_buckets(self):
    left = [Node_list() for x in range(self.maxLinks * 2 + 1)]
    right = [Node_list() for x in range(self.maxLinks * 2 + 1)]
    return left, right

  def calculate_gain(self, node):
    gain = 0
    own_value = self.currentSolution[node.index -1]
    for link in node.links:
      if own_value == self.currentSolution[link -1]:
        gain -= 1
      else:
        gain += 1
    node.gain = gain


  def calculate_gains(self, left, right):
    for node in self.Graph.nodes:
      own_value = self.currentSolution[node.index -1]
      self.calculate_gain(node)
      if own_value == 0:
        left[node.gain].append(node)
      else:
        right[node.gain].append(node)
    return left, right

  def swap_best(self, use_left, left_bucket, right_bucket):
    if use_left:
      current_bucket = left_bucket
    else:
      current_bucket = right_bucket
    for i in range(self.maxLinks, -self.maxLinks - 1, -1):
      if current_bucket[i].count > 0:
        node = current_bucket[i].pop()
        print(node.index)
        self.currentSolution[node.index - 1] = 1 - self.currentSolution[node.index - 1] #Swap value
        #Update neighbours
        for neighbour in [self.Graph.nodes[link - 1] for link in node.links]: 
          #Remove from the bucket it is in
          if neighbour.is_in_bucket:
            if self.currentSolution[neighbour.index - 1] == 0:
              left_bucket[neighbour.gain].remove_node(neighbour)
            else:
              right_bucket[neighbour.gain].remove_node(neighbour)

            #Recalculate gain and add to the correct bucket
            self.calculate_gain(neighbour)
            if self.currentSolution[neighbour.index - 1] == 0:
              left_bucket[neighbour.gain].append(neighbour)
            else:
              right_bucket[neighbour.gain].append(neighbour)
        self.cuts -= node.gain
        break
  
  def is_left_bucket_best(self, left_bucket, right_bucket):
    for i in range(self.maxLinks, -self.maxLinks - 1, -1):
      if len(left_bucket[i]) > 0:
        return True
      if len(right_bucket[i]) > 0:
        return False

  def fm_Pass(self):
    left, right = self.generate_buckets()
    left_bucket, right_bucket = self.calculate_gains(left, right)
    use_left = True ## self.is_left_bucket_best(left_bucket, right_bucket)
    bestSolution = copy.deepcopy(self.currentSolution) #Store a copy of the best solution
    print(f"Initial: {bestSolution}", f"Cuts: {self.cuts}")
    bestScore = self.cuts
    for i in range(len(self.Graph.nodes)):
        #Todo: keep track of best solution.
        self.swap_best(use_left, left_bucket, right_bucket)
        if(self.cuts < bestScore and i % 2 == 1): #Only look for better score every two swaps, because we need an equal amount of nodes in each group.
          print(bestSolution)
          bestSolution = copy.deepcopy(self.currentSolution)
          bestScore = self.cuts
        use_left = not use_left
    print(f"Best: {bestSolution}", f"Cuts: {bestScore}")
    print(f"Last: {self.currentSolution}", f"Cuts: {self.cuts}")

x = Model()
x.fm_Pass()